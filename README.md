# think-extend
The ThinkPHP 6 Extend Package  
ThinkExtend 是针对 ThinkPHP 6 版本封装的一些扩展方法类库，方便在构建 Web 应用的时候快速使用。
## 安装

> composer require qhweb/think-extend

## 包含扩展

* SM4国密加密扩展
* 汉字转拼音扩展
* 数据库操作扩展
* 文件、文件夹操作扩展
* 数据处理扩展，父级、子集、树形结构数据生成
* PDF转图片，ELEXS转HTML，WORD转HTML，自动保存内容图片
* 汉字分词扩展，用户关键字生成
* Sql文件处理扩展
* 图片压缩、裁剪处理，目前依托think-image实现

## 代码仓库

ThinkExtend 为 MIT 协议开源项目，安装使用或二次开发不受约束，欢迎 fork 项目。

部分代码来自互联网，若有异议可以联系作者进行删除。



* Gitee 仓库地址：https://gitee.com/wlx115/think-extend

## 使用说明

* ThinkExtend 需要 Composer 支持  
* 安装命令 ` composer require qhweb/think-extend`

#### 数据处理扩展方法
```php
/**
 * 一维数组转多维数据树
 * @param array $its 待处理数据
 * @param string $cid 自己的主键
 * @param string $pid 上级的主键
 * @param string $sub 子数组名称
 * @return array
 */
$result = arr2tree(array $its, string $cid = 'id', string $pid = 'pid', string $sub = 'sub');

/**
 * 一维数组转数据树表
 * @param array $its 待处理数据
 * @param string $cid 自己的主键
 * @param string $pid 上级的主键
 * @param string $path 当前 PATH
 * @return array
 */
$result = arr2table(array $its, string $cid = 'id', string $pid = 'pid', string $path = 'path');

/**
 * 获取数据树子ID集合
 * @param array $list 数据列表
 * @param mixed $value 起始有效ID值
 * @param string $ckey 当前主键ID名称
 * @param string $pkey 上级主键ID名称
 * @return array
 */
$result = getArrSubIds(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid');

/**
 * 获取数据的直接子集
 * @param array $list 数据列表
 * @param array $value 起始有效ID值
 * @param string $ckey 当前主键ID名称
 * @param string $pkey 上级主键ID名称
 * @return array
 */
$result = getArrSub(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid');

/**
 * 获取数据的所有子集
 * @param array $list 数据列表
 * @param array $value 起始有效ID值
 * @param string $ckey 当前主键ID名称
 * @param string $pkey 上级主键ID名称
 * @return array
 */
$result = getArrSubs(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid');

/**
 * 获取数据的父级
 * @param array $list 数据列表
 * @param array $value 起始有效ID值
 * @param string $ckey 当前主键ID名称
 * @param string $pkey 上级主键ID名称
 * @return array
 */
$result = getArrParent(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid')

/**
 * 获取数据的所有父级
 * @param array $list 数据列表
 * @param array $value 起始有效ID值
 * @param string $ckey 当前主键ID名称
 * @param string $pkey 上级主键ID名称
 * @return array
 */
$result = getArrParents(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid')

/**
 * 多维数组中查找数据
 * @param array $array 要查找的数据
 * @param string $key 查找的键值
 * @param string|array $value 查找的键值对应的值
 * @return array
 */
$result = getArrSearch(array $array,string $key='id',$value='');

/**** 静态方法使用 *****/
use qhweb\extend\DataExtend;
$result = DataExtend::方法名('参数1','参数2','..');

```
#### 拼音转换方法

```php
/**
 * 字符串转拼音
 * @param string $str 需要转换的字符串
 * @param boolean $frist  是否只取首字母
 * @param string $encoding 字符串编码
 * @return string
 */
$result = pinyin(string $str, bool $frist = false,string $encoding = 'utf-8')

```
#### 中文分词使用方法

```php
/**
 * SCWS中文分词
 * @param string $text 分词字符串
 * @param number $number 权重高的词数量(默认5个)
 * @param string $type 返回数组或字符串,默认字符串
 * @param string $delimiter 分隔符
 * @return string|array 字符串|数组
 */
$result = SplitWord($text = '', $number = 5, $type = true, $delimiter = ',');

```
#### UTF8加密算法

```php
// 字符串加密操作
$string = encode($content);

// 加密字符串解密
$content = decode($string);

//国密SM4加密
$string = encryptSm4($content,$key);

//国密SM4解密
$string = decryptSm4($content,$key);

```

#### OFFICE文件操作
>OFFICE文件的操作需要"PhpWord","PhpSpreadsheet","imagick扩展"的支持，请提前安装  
> composer require phpoffice/phpspreadsheet  
> composer require phpoffice/phpWord

```php
 /**
 * word转html
 * @param $source 需要转换的word源文件
 * @param $savepath 图片保存路径
 * @return array [状态，html,图片集]
 */
$string = doc2html($source,$savepath='upload');

/**
 * XLS转html
 * @param $source 需要转换的xls源文件
 * @param $savepath 图片保存路径
 * @return array [状态，html]
 */
$string = xls2html($source,$savepath='upload');

/**
 * PDF转图片
 * @param $source 需要转换的PDF源文件
 * @param $type 返回数据格式arr 为图片地址，html为图片代码<img src='...'/>
 * @param $savepath 图片保存路径
 * @return array [状态，html,图片集]
 */
$string = pdf2png($source,$type='arr',$savepath='upload');

```

#### 文件目录操作

```php
//格式化目录地址
$result = dir_path($path)
//创建目录
$result = dir_create($path, $mode = 0777)
//复制目录
$result = dir_copy($dir1, $dir2);
//移动目录
$result = dir_move($dir1, $dir2)
//删除目录（递归删除）
$result = dir_delete($path);
//判断文件、文件夹是否可写
$result = is_write($path);
```
#### 内容图片附件下载到本地

```php
use qhweb\extend\DownExtend;
/**
 * 查找内容中远程图片并下载到本地
 * @param string $content  查询内容
 * @param string $path  保存路径
 * @return string
 */
$content = DownExtend::downImage($content,$path='upload/remote/')

/**
 * 查找内容中远程附件并下载到本地
 * @param string $content  查询内容
 * @param string $path  保存路径
 * @return string
 */
$content = DownExtend::downLink($content='',$path='upload/remote/');
```

## 赞助打赏，请作者喝杯茶 ~

![赞助](http://thirdservice.yyinfos.com/static/pay.jpg)
