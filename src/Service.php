<?php
/*
* +----------------------------------------------------------------------
* |  Library for ThinkAdmin
* +----------------------------------------------------------------------
* | 版权所有 2015~2022 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
* +----------------------------------------------------------------------
* | 开源协议 ( https://mit-license.org )
* +----------------------------------------------------------------------
* | Copyright (c) 2022 by 青海西诚电子科技有限公司, All Rights Reserved. 
* +----------------------------------------------------------------------
* | gitee 仓库地址 ：https://gitee.com/wlx115/think-extend
* +----------------------------------------------------------------------
*/

declare (strict_types=1);

namespace qhweb;

use think\App;
use think\Container;

/**
 * 自定义服务基类
 * Class Service
 * @package think\admin
 */
abstract class Service
{
    /**
     * 应用实例
     * @var App
     */
    protected $app;

    /**
     * Service constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->initialize();
    }

    /**
     * 初始化服务
     */
    protected function initialize()
    {
    }

    /**
     * 静态实例对象
     * @param array $var 实例参数
     * @param boolean $new 创建新实例
     * @return static|mixed
     */
    public static function instance(array $var = [], bool $new = false)
    {
        return Container::getInstance()->make(static::class, $var, $new);
    }
}