<?php
/*
* +----------------------------------------------------------------------
* |  Library for ThinkAdmin
* +----------------------------------------------------------------------
* | 版权所有 2015~2022 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
* +----------------------------------------------------------------------
* | 开源协议 ( https://mit-license.org )
* +----------------------------------------------------------------------
* | Copyright (c) 2022 by 青海西诚电子科技有限公司, All Rights Reserved. 
* +----------------------------------------------------------------------
* | gitee 仓库地址 ：https://gitee.com/wlx115/think-extend
* +----------------------------------------------------------------------
*/

declare (strict_types=1);

use qhweb\extend\PinyinExtend;
use qhweb\extend\EncryptExtend;
use qhweb\extend\DataExtend;
use qhweb\extend\SplitWord;
use qhweb\extend\OfficeExtend;
use qhweb\extend\DirExtend;
if (!function_exists('arr2tree')) {
    /**
     * 一维数组转多维数据树
     * @param array $its 待处理数据
     * @param string $cid 自己的主键
     * @param string $pid 上级的主键
     * @param string $sub 子数组名称
     * @return array
     */
    function arr2tree(array $its, string $cid = 'id', string $pid = 'pid', string $sub = 'sub'){
        return DataExtend::arr2tree($its, $cid, $pid, $sub);
    }
}
if (!function_exists('arr2table')) {
    /**
     * 一维数组转数据树表
     * @param array $its 待处理数据
     * @param string $cid 自己的主键
     * @param string $pid 上级的主键
     * @param string $path 当前 PATH
     * @return array
     */
    function arr2table(array $its, string $cid = 'id', string $pid = 'pid', string $path = 'path'){
        return DataExtend::arr2table($its, $cid, $pid, $path);
    }
}
if (!function_exists('arr2table')) {
    /**
     * 获取数据树子ID集合
     * @param array $list 数据列表
     * @param mixed $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    function getArrSubIds(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'){
        return DataExtend::getArrSubIds($list, $value, $ckey, $pkey);
    }
}
if (!function_exists('getArrSub')) {
    /**
     * 获取数据的直接子集
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    function getArrSub(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'){
        return DataExtend::getArrSub($list, $value, $ckey, $pkey);
    }
}
if (!function_exists('getArrSubs')) {
    /**
     * 获取数据的所有子集
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    function getArrSubs(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'){
        return DataExtend::getArrSubs($list, $value, $ckey, $pkey);
    }
}
if (!function_exists('getArrParent')) {
    /**
     * 获取数据的父级
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    function getArrParent(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'){
        return DataExtend::getArrParent($list, $value, $ckey, $pkey);
    }
}
if (!function_exists('getArrParent')) {
    /**
     * 获取数据的所有父级
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    function getArrParents(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'){
        return DataExtend::getArrParents($list, $value, $ckey, $pkey);
    }
}
if (!function_exists('getArrParent')) {
    /**
     * 多维数组中查找数据
     * @param array $array 要查找的数据
     * @param string $key 查找的键值
     * @param string|array $value 查找的键值对应的值
     * @return array
     */
    function getArrSearch(array $array,string $key='id',$value=''){
        return DataExtend::getArrSearch($array, $key, $value);
    }

}
if (!function_exists('pinyin')) {
    /**
     * 字符串转拼音
     *
     * @param string $str 需要转换的字符串
     * @param boolean $frist  是否只取首字母
     * @param string $encoding 字符串编码
     * @return void
     */
    function pinyin(string $str, bool $frist = false,string $encoding = 'utf-8')
    {
        if($frist){
            return PinyinExtend::ShortPinyin($str, $encoding);
        }else{
            return PinyinExtend::Pinyin($str, $encoding);
        }
    }
}
if (!function_exists('SplitWord')) {
    /**
     * SCWS中文分词
     *
     * @param string $text 分词字符串
     * @param number $number 权重高的词数量(默认5个)
     * @param string $type 返回数组或字符串,默认字符串
     * @param string $delimiter 分隔符
     * @return string|array 字符串|数组
     */
    function SplitWord($text = '', $number = 5, $type = true, $delimiter = ','){
        return SplitWord::SplitWord($text,$number,$type,$delimiter);
    }
}
if (!function_exists('encryptSm4')) {
   /**
    * 国密SM4加密
    *
    * @param string $str 待加密数据
    * @param string $key 密钥
    * @return void
    */
    function encryptSm4(string $str, string $key = '')
    {
       return EncryptExtend::encrypt($str,$key);
    }
}

if (!function_exists('decryptSm4')) {
    /**
     * 国密SM4解密
     *
     * @param string $str 待解数据
     * @param string $key 密钥
     * @return void
     */
    function decryptSm4(string $str, string $key = '')
    {
       return EncryptExtend::decrypt($str,$key);
    }
}

if (!function_exists('doc2html')) {
    /**
     * word转html
     * @param $source 需要转换的word源文件
     * @param $savepath 图片保存路径
     * @return array [状态，html,图片集]
     */
    function doc2html(string $source, string $savepath = 'upload')
    {
       return OfficeExtend::doc2html($source,$savepath);
    }
}

if (!function_exists('xls2html')) {
    /**
     * XLS转html
     * @param $source 需要转换的xls源文件
     * @param $savepath 图片保存路径
     * @return array [状态，html]
     */
    function xls2html(string $source, string $savepath = 'upload')
    {
       return OfficeExtend::xls2html($source,$savepath);
    }
}

if (!function_exists('pdf2png')) {
    /**
     * PDF转图片
     * @param $source 需要转换的PDF源文件
     * @param $type 返回数据格式arr 为图片地址，html为图片代码<img src='...'/>
     * @param $savepath 图片保存路径
     * @return array [状态，html,图片集]
     */
    function pdf2png($source,$type='arr',$savepath = 'upload')
    {
       return OfficeExtend::pdf2png($source,$type,$savepath);
    }
}

if (!function_exists('dir_path')) {
     /**
     * 格式化目录地址
     * @param $path 目录地址
     * @return string
     */
    function dir_path($path)
    {
       return DirExtend::dirPath($path);
    }
}
if (!function_exists('dir_create')) {
    /**
    * 创建目录
    * @param $path  目录地址 
    * @return bool
    */
   function dir_create($path, $mode = 0777)
   {
      return DirExtend::dirCreate($path,$mode);
   }
}
if (!function_exists('dir_copy')) {
    /**
     * 复制目录
     * @param $dir1 目录源路径
     * @param $dir2 目录目标路径
     * @return bool
     */
    function dir_copy($dir1, $dir2)
    {
        return DirExtend::dirCopy($dir1, $dir2);
    }
}
if (!function_exists('dir_move')) {
    /**
     * 移动目录
     * @param $dir1 目录源路径
     * @param $dir2 目录目标路径
     * @return bool
     */
    function dir_move($dir1, $dir2)
    {
        return DirExtend::dirMove($dir1, $dir2);
    }
}
if (!function_exists('dir_delete')) {
    /**
     * 删除目录（递归删除）
     * @param $path 路径
     * @return bool
     */
    function dir_delete($path)
    {
        return DirExtend::dirDelete($path);
    }
}
if (!function_exists('is_write')) {
    /**
     * 检查读写权限
     * @param $path 目录或文件地址
     * @return string
     */
    function is_write($path)
    {
        return DirExtend::isWrite($path);
    }
}
