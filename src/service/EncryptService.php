<?php
/*
* +----------------------------------------------------------------------
* |  Library for ThinkAdmin
* +----------------------------------------------------------------------
* | 版权所有 2015~2022 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
* +----------------------------------------------------------------------
* | 开源协议 ( https://mit-license.org )
* +----------------------------------------------------------------------
* | Copyright (c) 2022 by 青海西诚电子科技有限公司, All Rights Reserved. 
* +----------------------------------------------------------------------
* | gitee 仓库地址 ：https://gitee.com/wlx115/think-extend
* +----------------------------------------------------------------------
*/

declare (strict_types=1);

namespace qhweb\service;

use qhweb\Service;
use qhweb\extend\EncryptExtend;
/**
 * 系统数据加密服务
 * Class EncryptService
 * @package qhweb\service
 */
class EncryptService extends Service
{

    static $key = 'GJwsXX_BzW=gJWJW';

    /**
     * @name: serarchSql
     * @desc: 列表模糊查询SQL语句生成
     * @author: qhweb
     * @param:	$fields	string|array	字段信息
     * @return:	$tag	string	字段链接符
    **/
    public static function serarchSql($fields,$tag = 'and',$data=[])
    {
        $sqls = [];
        $aesKey = self::$key;
        $gets = !empty($data) ? $data : request()->get();
        if(is_array($fields)){
            foreach ($fields as $field => $get) {
                if(isset($gets[$get]) && $gets[$get]){
                    $sqls[] ="AES_DECRYPT(UNHEX(`{$field}`),'{$aesKey}') like CONCAT('%','{$gets[$get]}','%')";
                }
            }
        }else{
            $sqls = ["AES_DECRYPT(UNHEX(`{$fields}`),'{$aesKey}') like CONCAT('%','{$gets[$get]}','%')"];
        }
        return count($sqls) > 1 ? implode(' '.$tag.' ',$sqls) : implode(' ',$sqls);
    }
    /**
     * 获取表单提交的加密数据
     *
     * @param array $data
     * @param  $encode 0解密，1加密
     * @return void
     */
    public static function entryptData($str='',$encode = 0)
    {
        
        if(empty($str)) return [];
        $str = mb_convert_encoding($str, 'UTF-8', 'UTF-8' );
        if ($encode) {
            return EncryptExtend::encrypt($str,self::$key);
        }else{
            $jsonData = EncryptExtend::decrypt($str,self::$key)??'';
            return !is_null(json_decode($jsonData,true)) ? json_decode($jsonData,true) : strval($jsonData);
        }
    }


    /**
     * 解密数据
     *
     * @param array $data  需要解密的数据
     * @param array $fields 需要解密的数据索引KEY
     * @return void
     */
    public static function decode(array $data, $fields = [],$key=''): array
    {
        $fields = $fields ?? ['sm4'=>[],'aes'=>[]];
        $safeKey = !empty($key) ? $key : self::$key;
        // SM4数据解密
        foreach ($fields['sm4'] as $val) {
            if (isset($data[$val]) && $data[$val]) {
                $decodeData = EncryptExtend::decrypt($data[$val],$safeKey)??'';
                if ($decodeData ) {
                    $data[$val] = !is_null(json_decode($decodeData, true)) ? json_decode($decodeData, true) : strval($decodeData);
                }
            }
            
        }
        // MYSQL数据库AES解密
        // foreach ($fields['aes'] as $key => $val) {
        //     if(isset($data[$key]) && $data[$key]){
        //         $data[$key] = static::aes_decode($data[$key]);
        //     }
        // }
        return $data;
    }
    /**
     * 批量加密数据
     *
     * @param array $data 需要加密的数据
     * @param array $fields 需要加密的数据索引KEY
     * @return void
     */
    public static function encode(array $data, $fields = [],$key=''): array
    {
        $fields = $fields ?? ['sm4'=>[],'aes'=>[]];
        $safeKey = !empty($key) ? $key : self::$key;
        // MYSQL数据库AES加密
        foreach ($fields['aes'] as $key => $val) {
             //只在数据存在的时候加密处理
            if(isset($data[$val]) && $data[$val]){
                $data[$key] = app()->db->raw("HEX(AES_ENCRYPT('{$data[$val]}','{$safeKey}'))");
            }
        }
        // SM4加密数据
        foreach ($fields['sm4'] as $val) {
            //只在数据存在的时候加密处理
            if(isset($data[$val]) && $data[$val]){
                if (is_array( $data[$val] )) {
                    $data[$val] = EncryptExtend::encrypt(json_encode( $data[$val]),$safeKey);
                } else {
                    $data[$val] = EncryptExtend::encrypt( $data[$val] ,$safeKey);
                }
            }
        }

        return $data;
    }

    /** 数据脱敏
     * @param $string 需要脱敏值
     * @param int $start 开始
     * @param int $length 结束
     * @param string $re 脱敏替代符号
     * @return bool|string
     * 例⼦:
     * dataDesensitization('131********', 3, 4); //131****9876
     * dataDesensitization('张三四', 0, -1); //**四
     */
    public static function dataDesensitization($string, $start = 0, $length = 0, $re = '*')
    {
        if (empty($string)) {
            return false;
        }
        $string = strval($string);
        $strarr = array();
        $mb_strlen = mb_strlen($string);
        while ($mb_strlen) { //循环把字符串变为数组
            $strarr[] = mb_substr($string, 0, 1, 'utf8');
            $string = mb_substr($string, 1, $mb_strlen, 'utf8');
            $mb_strlen = mb_strlen($string);
        }
        $strlen = count($strarr);
        $begin = $start >= 0 ? $start : ($strlen - abs($start));
        $end = $last = $strlen - 1;
        if ($length > 0) {
            $end = $begin + $length - 1;
        } elseif ($length < 0) {
            $end -= abs($length);
        }
        for ($i = $begin; $i <= $end; $i++) {
            $strarr[$i] = $re;
        }
        if ($begin > $end || $begin > $last || $end > $last) return $start.'-'.$begin.'-'.$end.'-'.$last;
        return implode('', $strarr);
    }
    /**
     * 整合 图片压缩 控制高宽 改格式
     * 图片名称 ，高，宽，图片存入路径,图片格式
     */
    public static function base64Img($imgSrc){
        $imgs = parse_url($imgSrc);
        $path = '.'.$imgs['path'];
        //压缩图片
        self::compressedImage($path,$path);
        //转换成base64
        $imgData = self::imgToBase64($path);
        //删除本地图片
        @unlink($path);
        return $imgData;
    }

    /**
     * desription 压缩图片
     * @param sting $imgsrc 图片路径
     * @param string $imgdst 压缩后保存路径
     */
    private static function compressedImage($imgSrc, $imgdst) {
        list($width, $height, $type) = getimagesize($imgSrc);
        $new_width = $width>300?300:$width;//图片宽度的限制
        $new_height =$height>400?400:$height;//自适应匹配图片高度
        switch ($type) {
            case 2:
                header('Content-Type:image/jpeg');
                $image_wp = imagecreatetruecolor($new_width, $new_height);
                $image = imagecreatefromjpeg($imgSrc);
                break;
            case 3:
                header('Content-Type:image/png');
                $image_wp = imagecreatetruecolor($new_width, $new_height);
                $image = imagecreatefrompng($imgSrc);
                break;
        }
        imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        //90代表的是质量、压缩图片容量大小
        imagejpeg($image_wp, $imgdst, 90);
        imagedestroy($image_wp);
        imagedestroy($image);
    }


    /**
     * 获取图片的Base64编码(不支持url)
     * @date 2021-04-9 16:02:11
     *
     * @param $img_file 传入本地图片地址
     *
     * @return string
     */
    private static function imgToBase64($img_file) {

        $img_base64 = '';
        if (file_exists($img_file)) {
            $app_img_file = $img_file; // 图片路径
            $img_info = getimagesize($app_img_file); // 取得图片的大小，类型等

            //echo '<pre>' . print_r($img_info, true) . '</pre><br>';
            $fp = fopen($app_img_file, "r"); // 图片是否可读权限

            if ($fp) {
                $filesize = filesize($app_img_file);
                $content = fread($fp, $filesize);
                $file_content = chunk_split(base64_encode($content)); // base64编码
                switch ($img_info[2]) {           //判读图片类型
                    case 1: $img_type = "gif";
                        break;
                    case 2: $img_type = "jpg";
                        break;
                    case 3: $img_type = "png";
                        break;
                }

                $img_base64 = 'data:image/' . $img_type . ';base64,' . $file_content;//合成图片的base64编码

            }
            fclose($fp);
        }

        return $img_base64; //返回图片的base64
    }
    /**
     * AES解密
     */
    public static function aes_decode($data,$key='')
    {
        $safeKey = !empty($key) ? $key : self::$key;
        return openssl_decrypt($data,'aes-256-ecb',$safeKey,OPENSSL_RAW_DATA);
    }
    /**
     * AES解密
    */
    public static function aes_encode($data,$key=''){
        $safeKey = !empty($key) ? $key : self::$key;
        $data = openssl_encrypt($data,'aes-256-ecb',$safeKey,OPENSSL_RAW_DATA);
        return $data;
    }
}