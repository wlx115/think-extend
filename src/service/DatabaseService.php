<?php
/*
* +----------------------------------------------------------------------
* |  Library for ThinkAdmin
* +----------------------------------------------------------------------
* | 版权所有 2015~2022 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
* +----------------------------------------------------------------------
* | 开源协议 ( https://mit-license.org )
* +----------------------------------------------------------------------
* | Copyright (c) 2022 by 青海西诚电子科技有限公司, All Rights Reserved. 
* +----------------------------------------------------------------------
* | gitee 仓库地址 ：https://gitee.com/wlx115/think-extend
* +----------------------------------------------------------------------
*/

declare (strict_types=1);

namespace qhweb\service;

use qhweb\Exception;
use qhweb\Service;

/**
 * 数据库管理服务
 * Class DatabaseService
 * @package qhweb\service
 */
class DatabaseService extends Service
{

    /**
     * 解析 查询条件
    * @param string|array $fields 查询条件  格式['field'=>value|condtion]
     * @return $this
     */
    /**
     * 解析 查询条件
     *
     * @param Model|string $dbQuery
     * @param [type] $fields
     * @return void
     */
    public function queryWhere($dbQuery, $fields)
    {
        foreach ($fields as $key => $val) {
            [$value, $condtion] = explode('|',$val);
            if (!empty($value)) {
                switch ($condtion) {
                    case 'include':
                        $dbQuery->whereLike($key,"%{$value}%");
                        break;
                    case 'notinclude':
                        $dbQuery->whereNotLike($key,"%{$value}%");
                        break;
                    case '=':
                    case '>=':
                    case '>':
                    case '<':
                    case '<=':
                    case '<>':
                        $dbQuery->where($key,$condtion,$value);
                        break;
                }
                [$dk, $qk] = explode($alias, $field);
            }
        }
        return $dbQuery;
    }

    /**
     * 创建数据表
     * @param string $name 数据表名
     * @param array $fields 表字段数据[['name'=>'fieldName','define'=>'archar(128) NULL','comment'=>'字段说明']]
     * @param string $title 表备注名
     * @throws \think\admin\Exception
     */
    public function createTable(string $name,string $title,array $fields=[])
    {
        $table = $this->getTable($name);
        try {
            $sql = "CREATE TABLE IF NOT EXISTS `{$table}` (
                `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
                `sort` int(11) DEFAULT 0 COMMENT '排序',
                `status` tinyint(1) DEFAULT 1 COMMENT '状态',
                `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
                `update_at` timestamp NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                PRIMARY KEY (`id`) USING BTREE
            ) ENGINE=InnoDB DEFAULT CHARSET={$this->config['charset']} COMMENT='{$title}';";
            // pr($sql);
            $this->app->db->query($sql);
            //加入自定义的表字段
            foreach ($fields as $field) {
                $this->createField($table,$field);
            }
        }catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * 修改数据表
     * @param string $name 数据表名
     * @param array $fields 表字段数据[['name'=>'fieldName','define'=>'archar(128) NULL','comment'=>'字段说明']]
     * @param string $title 表备注名
     * @throws \think\admin\Exception
     */
    public function updateTable(string $from,string $to)
    {
        $from = $this->getTable($from);
        $to = $this->getTable($to);
        $tables = $this->getTables();
        
        if (in_array($to, $tables)) {
            throw new Exception("数据表 {$to} 已存在！");
        } else {
            if (!in_array($from, $tables)) {
                throw new Exception("数据表 {$from} 不存在！");
            }else{
                try {
                    $this->app->db->execute("rename table {$from} to {$to};");
                } catch (Exception $e) {
                    throw $e;
                }
            }
        }
    }

    /**
     * 删除数据表
     * @param string $name 数据表名
     * @return bool
     */
    public function deleteTable(string $name)
    {
        $table = $this->getTable($name);
        try {
            $this->app->db->execute("DROP TABLE IF EXISTS `{$table}`");
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * 创建字段
     * @param string $table 数据表名
     * @param array $field 字段数据
     * @return bool
     */
    public function createField(string $table,array $field, array $tables = [])
    {
        $table = $this->getTable($table);
        $tables = $this->getTables();
        if (in_array($table, $tables)) {
            list($name,$define,$comment) = [$field['name'],$field['define'],$field['comment']??$field['title']];
            try {
                $this->app->db->execute("ALTER TABLE `{$table}` ADD COLUMN `{$name}` {$define} COMMENT '{$comment}' AFTER `id`");
            } catch (Exception $e) {
                throw $e;
            } 
        } else {
            throw new Exception("数据表 {$table} 不存在！");
        }
    }

    /**
     * 更新字段
     * @param string $table 数据表名
     * @param string $from 要修改的字段名
     * @param array $to 修改后的字段名[name,define,title]
     * @return bool
     */
    public function updateField(string $table,string $from,array $to=[])
    {
        $table = $this->getTable($table);
        $tables = $this->getTables();

        if (in_array($table, $tables)) {
            //第一步查询数据库表中的所有字段
            $fields = $this->app->db->getTableInfo($table,'fields');
            list($field,$define,$comment) = [$to['name'],$to['define'],$to['comment']??$to['title']];
            //判断是否存在要改的字段，不存在则新建字段
            if(!in_array($from,$fields)){
                $sql = "ALTER TABLE `{$table}` ADD COLUMN `{$field}` {$define} COMMENT '{$comment}'";
            }elseif(!in_array($field,$fields)){
                $sql = "ALTER TABLE `{$table}` CHANGE COLUMN `{$from}` `{$field}` {$define} COMMENT '{$comment}'";
            }else{
                throw new Exception("字段名已存在 {$field}");
            }
            try {
                $this->app->db->execute($sql);
            } catch (Exception $exception) {
                throw $exception;
            }
        } else {
            throw new Exception("数据表 {$table} 不存在！");
        }
    }

    /**
     * 删除字段
     * @param null $field 字段数据
     * @return bool
     */
    public function deleteField(string $table,string $field)
    {
        $table = $this->getTable($table);
        $fields = $this->app->db->getTableInfo($table,'fields');
        //判断是否存在要删除的字段
        if(in_array($field,$fields)){
            try {
                $this->app->db->execute("ALTER TABLE `{$table}` DROP COLUMN `{$field}`");
            } catch (Exception $exception) {
                throw $exception;
            }
        }
    }

    /**
     * 获取带表前缀的表名
     * @param string $name
     * @return array
     */
    protected function getTable(string $name)
    {
        $perfix = $this->app->config->get('database.connections.mysql.prefix');
        $lenth = strlen($perfix);
        return (substr($name,0,$lenth) == $perfix) ? $name : $perfix . $name;
    }

    /**
     * 获取数据库所有数据表
     * @return array [table, total, count]
     */
    protected function getTables(): array
    {
        $tables = [];
        foreach ($this->app->db->query("show tables") as $item) {
            $tables = array_merge($tables, array_values($item));
        }
        return $tables;
    }
}