<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace qhweb\extend;

/**
 * 下载处理扩展
 * Class DownExtend
 * @package think\admin\extend
 */
class DownExtend
{
    /**
     * 查找内容中远程图片并下载到本地
     *
     * @param string $content  查询内容
     * @param string $path  保存路径
     * @return string
     */
    public static function downImage(string $content='',string $path='upload/remote/'): string
    {
        if(empty($content)) return null;
        $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png|\.jpeg]))[\'|\"].*?[\/]?>/";
        preg_match_all($pattern,$content,$match);
        if( isset($match[1]) && count($match[1]) >= 0 ){
            foreach ($match[1] as $url) {
                //判断是否远程图片
                if(preg_match("/^http(s)?:\\/\\/.+/",$url) && parse_url($url,PHP_URL_HOST) != $_SERVER['HTTP_HOST']){
                    $filename = pathinfo($url, PATHINFO_BASENAME);
                    if(static::downloadFile($url,$path,$filename)){
                        $content = str_replace($url,$path.$filename,$content);
                    }
                }
            }
        }
        return $content;
    }
    
    /**
     * 查找内容中远程附件并下载到本地
     * @param string $content  查询内容
     * @param string $path  保存路径
     * @return string
     */
    public static function downLink(string $content='',string $path='upload/remote/'): string
    {
        if(empty($content)) return null;
        $pattern="/<[a|A].*?href=[\'|\"](.*?(?:[\.doc|\.docx|\.xls|\.xlsx|\.zip|\.rar|\.pdf|\.ppt]))[\'|\"].*?[\/]?>/";
        preg_match_all($pattern,$content,$match);
        if( isset($match[1]) && count($match[1]) >= 0 ){
            foreach ($match[1] as $url) {
                //判断是否远程图片
                if(preg_match("/^http(s)?:\\/\\/.+/",$url) && parse_url($url,PHP_URL_HOST) != $_SERVER['HTTP_HOST']){
                    $filename = pathinfo($url, PATHINFO_BASENAME);
                    if(static::downloadFile($url,$path,$filename)){
                        $content = str_replace($url,$path.$filename,$content);
                    }
                }
            }
        }
        return $content;
    }
    /**
     * 下载到本地
     *
     * @param string $url  远程地址
     * @param string $path  保存地址
     * @param string $filename  文件名
     * @return bool
     */
    private static function downloadFile($url, $path='upload/remote/',$filename='')
    {
        if(!is_dir($path)) mkdir($path,0755);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file = curl_exec($ch);
        curl_close($ch);
        if($file){
            static::saveAsFile($url, $file, $path,$filename);
            return true;
        }else{
            return false;
        }
    }
    /**
     * 保存内容到文件
     *
     * @param string $score  文件路径
     * @param string $str  文件内容
     * @param string $path 保存路径
     * @param string $filename  文件名
     * @return void
     */
    private static function saveAsFile($score, $str, $path, $filename='')
    {
        //分析地址获取文件名
        if(empty($filename)){
            $filename = pathinfo($score, PATHINFO_BASENAME);
        }
        //生成文件并写入内容
        $resource = fopen($path . $filename, 'a');
        fwrite($resource, $str);
        fclose($resource);
    }
}