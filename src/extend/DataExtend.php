<?php
/*
* +----------------------------------------------------------------------
* |  Library for ThinkAdmin
* +----------------------------------------------------------------------
* | 版权所有 2015~2022 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
* +----------------------------------------------------------------------
* | 开源协议 ( https://mit-license.org )
* +----------------------------------------------------------------------
* | Copyright (c) 2022 by 青海西诚电子科技有限公司, All Rights Reserved. 
* +----------------------------------------------------------------------
* | gitee 仓库地址 ：https://gitee.com/wlx115/think-extend
* +----------------------------------------------------------------------
*/

declare (strict_types=1);

namespace qhweb\extend;

/**
 * 数据处理扩展
 * Class DataExtend
 * @package qhweb\extend
 */
class DataExtend
{
    /**
     * 一维数组转多维数据树
     * @param array $its 待处理数据
     * @param string $cid 自己的主键
     * @param string $pid 上级的主键
     * @param string $sub 子数组名称
     * @return array
     */
    public static function arr2tree(array $its, string $cid = 'id', string $pid = 'pid', string $sub = 'sub'): array
    {
        [$tree, $its] = [[], array_column($its, null, $cid)];
        foreach ($its as $it) isset($its[$it[$pid]]) ? $its[$it[$pid]][$sub][] = &$its[$it[$cid]] : $tree[] = &$its[$it[$cid]];
        return $tree;
    }

    /**
     * 一维数组转数据树表
     * @param array $its 待处理数据
     * @param string $cid 自己的主键
     * @param string $pid 上级的主键
     * @param string $path 当前 PATH
     * @return array
     */
    public static function arr2table(array $its, string $cid = 'id', string $pid = 'pid', string $path = 'path'): array
    {
        $call = function (array $its, callable $call, array &$data = [], string $parent = '') use ($cid, $pid, $path) {
            foreach ($its as $it) {
                $ts = $it['sub'] ?? [];
                unset($it['sub']);
                $it[$path] = "{$parent}-{$it[$cid]}";
                $it['spc'] = count($ts);
                $it['spt'] = substr_count($parent, '-');
                $it['spl'] = str_repeat('ㅤ├ㅤ', $it['spt']);
                $it['sps'] = ",{$it[$cid]},";
                array_walk_recursive($ts, function ($val, $key) use ($cid, &$it) {
                    if ($key === $cid) $it['sps'] .= "{$val},";
                });
                $it['spp'] = arr2str(str2arr(strtr($parent . $it['sps'], '-', ',')));
                $data[] = $it;
                if (empty($ts)) continue;
                $call($ts, $call, $data, $it[$path]);
            }
            return $data;
        };
        return $call(static::arr2tree($its, $cid, $pid), $call);
    }

    /**
     * 获取数据树子ID集合
     * @param array $list 数据列表
     * @param mixed $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrSubIds(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $ids = [intval($value)];
        foreach ($list as $vo) if (intval($vo[$pkey]) > 0 && intval($vo[$pkey]) === intval($value)) {
            $ids = array_merge($ids, static::getArrSubIds($list, intval($vo[$ckey]), $ckey, $pkey));
        }
        return $ids;
    }

    /**
     * 获取数据的直接子集
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrSub(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        // pr($list);
        $subs=array();
        foreach($list as $vo) if (intval($vo[$pkey]) === intval($value)) {
            $subs = array_merge($subs,[$vo]);
        }
        return $subs;
    }

    /**
     * 获取数据的所有子集
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrSubs(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $subs=array();
        foreach($list as $vo) if (intval($vo[$pkey]) === intval($value)) {
            $subs = array_merge($subs,[$vo],static::getArrSubs($list, intval($vo[$ckey]), $ckey, $pkey));
        }
        return $subs;
    }

    /**
     * 获取数据的父级
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrParent(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $arrs=array();
        foreach($list as $vo) if (intval($vo[$ckey]) > 0 && intval($vo[$ckey]) === intval($value)) {
            return $vo;break;
        }
        return $arrs;
    }

    /**
     * 获取数据的所有父级
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrParents(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $arrs=array();
        foreach($list as $vo) if (intval($vo[$ckey]) > 0 && intval($vo[$ckey]) === intval($value)) {
            $arrs = array_merge($arrs,[$vo],static::getArrParents($list, intval($vo[$pkey]), $ckey, $pkey));
        }
        return $arrs;
    }

    /**
     * 多维数组中查找数据
     * @param array $array 要查找的数据
     * @param string $key 查找的键值
     * @param string|array $value 查找的键值对应的值
     * @return array
     */
    public static function getArrSearch(array $array,string $key='id',$value=''): array
    {
        $newArray = [];
        $valArr = is_array($value) ? : explode(',',(string) $value);
        foreach($array as $keyp=>$valuep){
            if(in_array($valuep[$key],$valArr)){
                array_push($newArray,$array[$keyp]);
            }
        }
        return $newArray;
    }  
}