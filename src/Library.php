<?php
/*
* +----------------------------------------------------------------------
* |  Library for ThinkAdmin
* +----------------------------------------------------------------------
* | 版权所有 2015~2022 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
* +----------------------------------------------------------------------
* | 开源协议 ( https://mit-license.org )
* +----------------------------------------------------------------------
* | Copyright (c) 2022 by 青海西诚电子科技有限公司, All Rights Reserved. 
* +----------------------------------------------------------------------
* | gitee 仓库地址 ：https://gitee.com/wlx115/think-extend
* +----------------------------------------------------------------------
*/

declare (strict_types=1);

namespace qhweb;

use Closure;
use think\App;
use think\Request;
use think\Service;
use qhweb\service\EncryptService;
/**
 * 模块注册服务
 * Class Library
 * @package think\admin
 */
class Library extends Service
{
    /**
     * 组件版本号
     */
    const VERSION = '2.0.1';

    /**
     * 静态应用实例
     * @var App
     */
    public static $sapp;

    /**
     * 启动服务
     */
    public function boot()
    {
        // 静态应用赋值
        static::$sapp = $this->app;
    }

    /**
     * 初始化服务
     */
    public function register()
    {
         // 终端 HTTP 访问时特殊处理
         if (!$this->app->request->isCli()) {
            // 注册路由中间件
            $this->app->middleware->add(function (Request $request, Closure $next) {
               
                // SM4 数据加密传输处理
                $encrypt = $request->only(['encryptData'],'all','decryptSm4');
                if(isset($encrypt['encryptData'])){
                    $request->encryptData = !is_null(json_decode($encrypt['encryptData'],true)) ? json_decode($encrypt['encryptData'],true) : strval($encrypt['encryptData']);
                }
                
                //GET传递参数格式话
                $filter = json_decode(htmlspecialchars_decode($request->get('filter','')),true);
                if ($filter) {
                    $get = $request->get();
                    $op =  json_decode(htmlspecialchars_decode($request->get('op')),true);
                    $request->withGet(array_merge($get,['filter' => $filter, 'op' => $op]));
                }
                return $next($request);
            },'route');
        }
    }
}